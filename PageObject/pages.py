# -*- coding: utf-8 -*-
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException

from PageObject.base import Base
from Locators.locators import *


class LogInPage(Base):

    """ Page object class for log in """
    
    def wait_for_sign_in_to_load(self):
        self.wait_page_to_load(LogInPageLocators.MENU_BTN)

    def fill_login_from(self, email, passw):
        self.find_element(*LogInPageLocators.EMAIL_FLD).send_keys(email)
        self.find_element(*LogInPageLocators.PASS_FLD).send_keys(passw)

    def submit_login(self):
        self.click_element_and_wait(ToDosPageLocators.GREETINGS_TXT, *LogInPageLocators.SIGH_IN_BTN)

class ToDosPage(Base):

    """ Page object class for To Do"""
    
    def click_create_todo(self):
        self.click_element_and_wait(NewToDoFormPageLocators.TITLE_FLD, *ToDosPageLocators.NEW_TODO_BTN)

    def check_todo_created(self, todo_text):
        el = self.generate_locator("td", todo_text)
        try:
            self.find_element(el)
        except (NoSuchElementException):
            print("New To Do Not Found!")
            raise


class CreateToDoPage(Base):

    """ Page object class for creating To Do"""
    
    def create_todo(self, title, date):
        self.find_element(*NewToDoFormPageLocators.TITLE_FLD).send_keys(title)
        self.find_element(*NewToDoFormPageLocators.DATE_FLD).send_keys(date)
        self.click_element_and_wait(LogInPageLocators.MEDIUM_PRIORITY, *LogInPageLocators.PRIORITY_FLD)
        self.find_element(*NewToDoFormPageLocators.MEDIUM_PRIORITY).click()
        self.click_element_and_wait(LogInPageLocators.GREETINGS_TXT, *LogInPageLocators.CREATE_BTN)
