# -*- coding: utf-8 -*-
import json
import time
import datetime
import os.path

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class Base(object):

    """ Basic class for pages """

    def __init__(self, driver):
        self.driver = driver

    @staticmethod
    def open_data_json():
        with open('./TestData/user_data.json', 'r') as f:
            file = json.load(f)
            data = dict(CHROME_PATH = file["CHROME_PATH"], URL=file['URL'], EMAIL=file['EMAIL'], PASSW=file['PASS'])
        return data

    @staticmethod
    def setup_with_headless(chrome_path):
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-gpu')
        options.add_argument('--screen-size=1366x800')
        options.add_argument('window-size=1366x800')
        options.add_argument('start-maximized')
        des_cap = DesiredCapabilities.CHROME
        des_cap['loggingPrefs'] = {'browser': 'ALL'}
        driver = webdriver.Chrome(chrome_path,
            chrome_options=options, desired_capabilities=des_cap)
        return driver

    def find_element(self, *locator):
        return self.driver.find_element(*locator)

    def get_current_url(self):
        return self.driver.current_url

    def wait_page_to_load(self, locator, seconds=20):
        try:
            WebDriverWait(self.driver, seconds).until((EC.visibility_of_element_located(locator)))
        except TimeoutException:
            self.screenshot("page_didnt_load")
            raise AssertionError("Page didn't load. Locator:", locator)

    def screenshot(self, name):
        name = "screenshot_{}.png".format(name)
        self.driver.save_screenshot(name)

    def click_element(self, *locator):
        self.find_element(*locator).click()

    def click_element_and_wait(self, wait_locator, *btn_locator):
        self.find_element(*btn_locator).click()
        try:
            WebDriverWait(self.driver, 20).until((EC.visibility_of_element_located(wait_locator)))
        except TimeoutException:
            self.screenshot("wait_error")
            raise AssertionError("The next page did not opened. Locator:", wait_locator)

    def get_element_text(self, *locator):
        return self.find_element(*locator).text

    def generate_locator(self, tag, text):
        return f"//{tag}[contains(text(),'{text}')]"

