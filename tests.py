import pytest

from Helpers.api_helper import Api_Helper
from PageObject.base import Base
from PageObject.pages import *
from Locators.locators import *


class TestTask():

    todo_text = "test01"
    date = "14/02/2020"

    def setup(self):
        self.data = Base.open_data_json()
        self.driver = Base.setup_with_headless(self.data["CHROME_PATH"])
        self.login_page = LogInPage(self.driver)
        self.todo_page = ToDosPage(self.driver)
        self.create_page = CreateToDoPage(self.driver)
        url = self.data["URL"]
        self.user_email = self.data["EMAIL"]
        self.passw = self.data["PASSW"]
        self.driver.get(url)

    def teardown(self):
        self.driver.quit()

    def test_log_in_test_and_create_new_todo(self):
        self.login_page.wait_for_sign_in_to_load()
        self.login_page.fill_login_from(self.user_email, self.passw)
        self.login_page.submit_login()
        self.todo_page.click_create_todo()
        self.create_page.create_todo(self.todo_text, self.date)
        self.todo_page.check_todo_created(self.todo_text)

