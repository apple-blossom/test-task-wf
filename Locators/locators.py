# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By


class LogInPageLocators(object):

    """Locators for log in page"""

    SIGH_IN_BTN_LINK = (By.XPATH, "//a[contains(text(),'Sign in')]")
    EMAIL_FLD = (By.ID, "email")
    PASS_FLD = (By.ID, "password")
    SIGH_IN_BTN = (By.XPATH, "//button[contains(text(),'Sign in')]")
  
class ToDosPageLocators(object):

    """Locators for TO DO page"""

    GREETINGS_TXT = (By.XPATH, "//li[contains(text(),'Hi')]")
    NEW_TODO_BTN = (By.XPATH, "//a[contains(text(),'Create a todo')]")


class NewToDoFormPageLocators(object):

    """Locators for new TO DO form """

    TITLE_FLD = (By.ID, "title")
    DATE_FLD = (By.ID, "dueDate")
    PRIORITY_FLD = (By.ID, "priority")
    MEDIUM_PRIORITY = (By.XPATH, "//option[contains(text(),'Medium')]")
    CREATE_BTN = (By.XPATH, "//li[contains(text(),'Create')]")

  
